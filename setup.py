# Copyright 2021 Henix, henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage
#
# Local install (one of these, depending on context):
#
#     pip3 install --upgrade .
#     pip3 install -e .
#     pip3 install --user --no-use-pep517 -e .
#
# Build and publish (assuming a proper ~/.pypirc)
#
#     rm -r build/ dist/
#     python3 setup.py bdist_wheel [upload -r local]
#     twine upload [--repository testpypi] dist/*

from setuptools import setup, find_namespace_packages
from os import environ

with open('VERSION', 'r') as fv:
    VERSION = fv.read()
with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name=environ.get('PACKAGE_NAME', 'opentf-agent'),
    version=VERSION,
    description='OpenTestFactory Orchestrator Agent',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://opentestfactory.org/guides/agent.html',
    author='Martin Lafaix',
    author_email='mlafaix@henix.com',
    maintainer='Henix',
    maintainer_email='opentestfactory@henix.com',
    packages=find_namespace_packages(include=['opentf.*']),
    package_data={'': ['*.yaml']},
    include_package_data=True,
    install_requires=[
        'requests>=2.25',
    ],
    entry_points={'console_scripts': ['opentf-agent=opentf.agent:main']},
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries',
        'License :: OSI Approved :: Apache Software License',
    ],
    license='Apache Software License (https://www.apache.org/licenses/LICENSE-2.0)',
    python_requires='>= 3.7.0',
)
